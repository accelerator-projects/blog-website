#Authored: Harry
#Assisted: Josh
#Date: 22/10/18

from flask import Flask, render_template, request
import ast

app = Flask(__name__)

# Opens the text file containing all the blog information and separates it into a list of lists.
# The text file contains the title, author, creation date and blog content of each blog post.
# This information is placed into a list separating each of these values. These lists are then
# placed into a list that holds all the blog posts as shown below:
# [[title1,author1,date1,content1],[title2,author2,date2,content2]...]

# Return:
# A list of lists containing all the blog information.

def blogs():
    output=open('blog_content.txt','r')
    blog_array=[]
    i=0
    j=0
    l=""
    for k in output:
        if j==0:
            blog_array.append([])
            blog_array[i].append(k)
            j+=1
            continue
        if k=="---\n":
            blog_array[i].append(l)
            i+=1
            j=0
            l=""
            continue
        if j>2:
            l+=k
            continue
        blog_array[i].append(k)
        j+=1
    #print(blog_array)
    return blog_array

# Reads in all of the comments related to the received blog post and returns them as a list.

# Variables:
# x: the name of the blog post

#Returns:
#comments: the array containing all the comments related to the blog post

def commentsAdd(x):
    file_name="comments_"+x+".txt"
    output=open(file_name,'r')
    comments=[]
    comments.append([])
    i=0
    j=""
    for k in output:
        if k=="---\n":
            comments[i].append(j)
            comments.append([])
            i+=1
            j=""
            continue
        j+=k
    del comments[-1]
    return comments

# Creates the initial homepage when people first open the website.
# It reads in the text file containing all the blog information in the order of
# author -> title -> date -> blog content all written on new lines. Each different
# piece of blog information is separated by --- for reading help. The information is
# stored in a list of lists where each element of the first list contains a list holding
# the blog information. If no blogs are found, it returns an empty list. The homepage
# is then created and the list is passed to the html document.

#Variables:
#blog_array: the list of lists containing all the blog information

#Return:
# Opens the homepage html and sends the list containing the blog information
@app.route('/')
def home():
    try:
        blog_array=blogs()
    except FileNotFoundError:
        blog_array=[]
    return render_template('home.html',blog_array=blog_array)

# Creates the submission page where people can create blog posts.
# Return:
# Opens the submission page html
@app.route('/subpage',methods=['GET','POST'])
def subpage():
    return render_template('submission.html')

# Opens the relevant blog post page.
# It creates the list of lists containing all the blog information as described
# in function home(). The function receives an id value which describes where
# in the blog list it is. The list needs to be reversed as the id was received
# from a reversed list. Then the correct blog information is identified and
# using that information, the comments corresponding to that blog post are read
# into a list. This is all then sent to the page html document when it is loaded.

# Variables:
# blog: the list of lists containing all the blog information
# id: the position of the list containing the desired blog post in the first list
#     of blog
# given_blog: the list containing the information about the desired blog post
# comments: the list containing the comments that have previously been made on the
#           desired blog post

#Returns:
# Opens the page html document and sends given_blog, id and comments into it.

@app.route('/page',methods=['POST'])
def page():
    blog=blogs()
    blog.reverse()
    id = request.form['id']
    given_blog=blog[int(id)]
    try:
        comments=commentsAdd(given_blog[0])
    except FileNotFoundError:
        comments=[]
    return render_template('page.html',blog=given_blog,comments=comments,id=id)

# Adds a created blog post to the text file containing all the blog information
# and returns a new list of lists created as described in home() to the home
# page document.

# Variables:
# blog_array: list of lists containing all the blog information.

# Returns:
# Opens the homepage and returns list of lists containing all the blog information
# including the newly created blog post with it.

@app.route('/submit', methods=['POST'])
def submit():
    output = open('blog_content.txt', 'a')
    output.write(request.form['title'].rstrip("\n") +"\n")
    output.write(request.form['author'].rstrip("\n") +"\n")
    output.write(request.form['date'].rstrip("\n") +"\n")
    output.write(request.form['text'].rstrip("\n") +"\n")
    output.write("---\n")
    output.close()
    blog_array=blogs()
    return render_template('home.html',blog_array=blog_array)

# Receives the comments from a blog post and writes them to the relevant text file.
# Variables:
# comments: A list containing all the different comments related to the particular
#           blog post.
# blog: list of lists that contains all the blog information
# file_name: the name of the comments text file. This is made up of two strings that
#            are always the same and the title of the blog post the comments are
#            referring to

# Returns:
# Opens the home page and sends blog with it.

@app.route('/homeCom',methods=['POST','GET'])
def homeCom():
    comments=request.form['comment']
    blog=blogs()
    blog.reverse()
    file_name="comments_"+blog[int(request.form['id'])][0]+".txt"
    output=open(file_name,'w')
    x=""
    for i in comments:
        if i==",":
            output.write(x+"\n")
            output.write("---\n")
            x=""
            continue
        x+=i
    output.write(x+"\n")
    output.write("---\n")
    blog.reverse()
    return render_template('home.html',blog_array=blog)

@app.route('/deleteBlog',methods=['POST'])
def deleteBlog():
    blog=request.form['blog']
    output = open('blog_content.txt', 'w')
    test=0
    x=""
    xx=""
    for i in blog:
        if test==3:
            if i=="|":
                xx=","
                continue
            if xx==",":
                output.write(x+"\n")
                output.write("---\n")
                x=""
                test=0
                xx=""
                continue
            x+=i
            continue
        if i==",":
            output.write(x+"\n")
            x=""
            test+=1
            continue
        x+=i
    output.write(x+"\n")
    output.write("---\n")
    output.close()
    blog_array=blogs()
    #for i in blog:
    #    output.write(i[0]+"\n")
#        output.write(i[1]+"\n")
#        output.write(i[2]+"\n")
#        output.write(i[3]+"\n")
#        output.write("---\n")
    return render_template('home.html',blog_array=blog_array)

# From an inputted string, this searches through the list of lists containing all
# the blog information and if the string is in either the title, author or blog
# content, it adds the title/author name/title respectively to a list. The list
# then has its duplicates removed and is sent to the search html page.

# Variables:
# search: the string inputted by the user
# blog: the list of lists containing all the blog information
# x: the list containing the titles/author names that matched the inputted string

#Returns:
# Opens the search html page and sends x and authors as 0.

@app.route('/search',methods=['POST'])
def search():
    search = request.form['search']
    blog=blogs()
    x=[]
    for i in blog:
        if search.lower() in i[0].lower():
            x.append(i[0])
        if search.lower() in i[1].lower():
            x.append(i[1])
        if search.lower() in i[3].lower():
            x.append(i[0])
    x=list(set(x))
    authors=0
    return render_template('search.html',result=x,authors=authors)#,blog=blog)

# Takes in the search result that the user selected and returns the relevant page.
# If the user chose the title of a blog, the comments related to that blog are
# loaded into an array and sent to the page html document. If the user chose an
# author's name, all of the blogs posted by that author are added to a list that
# is then passed into the search html document.

# Variables:
# blog: list of lists containing the blog information
# x: list that may contain the blogs written by the author the user selected
# id: position in blog of the blog post the user selected
# given_blog: the list containg the information on the blog the user selected
# comments: the array containing the comments related to the blog the user selected

# Return:
# 1: Opens the page html document and sends blog, comments and id with it
# 2: Opens the search html document and sends x and authors equal to 0

@app.route('/searchResult',methods=['POST'])
def searchResult():
    blog=blogs()
    result=request.form['blah']
    print(result)
    x=[]
    for i in range(len(blog)):
        if result.lower() in blog[i][0].lower():
            id=i
            given_blog=blog[i]
            try:
                comments=commentsAdd(given_blog[0])
            except FileNotFoundError:
                comments=[]
            return render_template('page.html',blog=given_blog,comments=comments,id=id)
        if result.lower() in blog[i][1].lower():
            for j in blog:
                if result.lower() in j[1].lower():
                    x.append(j[0])
            return render_template('search.html',result=x,authors=0)

# Creates a list containing the names of all the authors that have made blog posts, removes
# the duplicates and sends them to the search html page.

# Variables:
# blog: list of lists containing all the blog information
# authors: the list containing all the names of the authors

#Return:
# Opens the search html document and sends the authors list and sets result equal to 0

@app.route('/authorsPage',methods=['GET'])
def authorsPage():
    blog=blogs()
    authors=[]
    for i in blog:
        authors.append(i[1])
    authors=list(set(authors))
    return render_template('search.html',result=0,authors=authors)

if __name__=='__main__':
    app.run(debug=True)
